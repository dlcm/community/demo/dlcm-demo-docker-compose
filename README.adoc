= DLCM Demo Docker Compose

image::DLCM-logo.png[DLCM Logo, 200]

Docker compose files for deploying demo environments of DLCM solution

== DLCM Architecture

image::DLCM-architecture.png[DLCM Architecture,height=250]

== Demo settings
* Set environment variable `IP_ADDRESS`
```
# For local usage
export IP_ADDRESS=127.0.0.1
# For external usage
export IP_ADDRESS=<host IP address>
```

TIP: For external usage, replace in the following URLs `localhost` by the `IP address`.

== DLCM Prerequisites

image::DLCM-prerequisites.png[DLCM Prerequisites,height=250]

=== Installation volumes & containers
* Run the following commands to deploy the stack of prerequisites
```
cd common-infra
./prepare-prerequisites.sh
docker compose up -d
```

=== Check if the services are running
* The Maria DB server: `mysql -P 3316 -u root`
* The Elasticserch engine: http://localhost:9210/
* The ActiveMQ broker: http://localhost:8171/admin (user: `admin`/`admin`)
* The ClamAV server
* The FITS servers: http://localhost:8888/fits-1.2.1/ or http://localhost:8888/fits-1.2.1/version
* The configuration server: http://localhost:8897/application/main
.. Authorization configuration: http://localhost:8897/authorization/default/authorization
.. DLCM full deployment configuration: http://localhost:8897/dlcm-full/default/dlcm-full
.. DLCM light deployment configuration: http://localhost:8897/dlcm-light/default/dlcm-light
.. DLCM standalone deployment configuration: http://localhost:8897/dlcm-standalone/default/dlcm-standalone
* The authorization server: http://localhost:10111/info
* The Shibboleth simulator: http://localhost:10110/shiblogin

== DLCM Full Deployment

image::DLCM-full.png[DLCM Full Deployment, height=250]

=== Installation volumes & containers
* Run the following commands to deploy the stack of prerequisites
```
cd dlcm-full
./prepare-environment.sh
docker compose up -d
```

=== Check if the services are running
* The backend servers
.. The administration: http://localhost:16115/dlcm/info
.. The Ingestion: http://localhost:16116/dlcm/info
.. The Storagion 1: http://localhost:16117/dlcm/info
.. The Storagion 2: http://localhost:16119/dlcm/info
.. The Accession: http://localhost:16118/dlcm/info
* The portal: http://localhost:1081
** The version info: http://localhost:1081/assets/frontend-version.json

== DLCM Light Deployment

image::DLCM-light.png[DLCM Light Deployment, height=250]

=== Installation volumes & containers
* Run the following commands to deploy the stack of prerequisites
```
cd dlcm-light
./prepare-environment.sh
docker compose up -d
```

=== Check if the services are running
* The backend servers
.. The administration: http://localhost:16125/dlcm/info
.. The Storagion 1: http://localhost:16127/dlcm/info
.. The Storagion 2: http://localhost:16129/dlcm/info
* The portal: http://localhost:1082
** The version info: http://localhost:1082/assets/frontend-version.json

== DLCM Standalone Deployment

image::DLCM-standalone.png[DLCM Standalone, height=250]

=== Installation volumes & containers
* Run the following commands to deploy the stack of prerequisites
```
cd dlcm-standalone
./prepare-environment.sh
docker compose up -d
```

=== Check if the services are running
* The DLCM Solution: http://localhost:16135/dlcm/info
* The portal: http://localhost:1083
** The version info: http://localhost:1083/assets/frontend-version.json

== DLCM Multiple-Storage Deployment

image::DLCM-multi-storage.png[DLCM Multi-Storage Deployment, height=250]

=== Installation volumes & containers
* Run the following commands to deploy the stack of prerequisites
```
cd dlcm-multi-storage
./prepare-environment.sh
docker compose up -d
```

=== Check if the services are running
* The backend servers
.. The administration: http://localhost:16145/dlcm/info
.. The Ingestion: http://localhost:16146/dlcm/info
.. The Storagion 1: http://localhost:16141/dlcm/info
.. The Storagion 2: http://localhost:16142/dlcm/info
.. The Storagion 3: http://localhost:16143/dlcm/info
.. The Storagion 4: http://localhost:16144/dlcm/info
.. The Accession: http://localhost:16148/dlcm/info
* The portal: http://localhost:1084
** The version info: http://localhost:1084/assets/frontend-version.json


== Demo Data Loading

=== Get Docker image
```
docker pull registry.gitlab.unige.ch/dlcm/community/demo/dlcm-demo-docker-compose/dlcm-demo-data:<version>
```

=== Run the admin data loading
```
cd <deployment>/properties
export TOKEN=<root-token>
docker run --network="host" --env=TOKEN --mount="type=bind,source=<absolute path>/dlcm-demo-data.properties,target=/dlcm-it-tests.properties,readonly" --rm -it dlcm-demo-data:<version>
```

=== Run the data loading
```
cd <deployment>/properties
export TOKEN=<root-token>
export IT_TESTS=<tests to load data> <1>
export IT_OPTIONS=-Ddlcm.demo-data.enabled=true
docker run --network="host" --env=TOKEN --env=IT_TESTS --env=IT_OPTIONS --mount="type=bind,source=<absolute path>/dlcm-demo-data.properties,target=/dlcm-it-tests.properties,readonly" --rm -it dlcm-demo-data:<version>
```
<1> Tests to load data:
. `DepositsDemoIT` To create deposits and archive them
. `MetadataEditionDepositsDemoIT` To edit existing archives and submit them
. `CollectionsDemoIT` To create archive collections and archive them
. `CollectionsOfCollectionsDemoIT` To create archive collections of collections and archive them

== Dataset Loading

=== Get dataset samples
* Get the sample data in a directory for datasets
```
git clone https://gitlab.unige.ch/dlcm/community/demo/dlcm-demo-datasets.git
```
* To get more datasets
```
cd dlcm-demo-datasets
git checkout full-set
```

=== Get the batch tool
* Download the `DLCM Tools` JAR file:
** for `full` deployment http://localhost:16115/dlcm/docs/DLCM-ToolsGuide.html#dlcm-tools-get[DLCM Tools]
** for `light` deployment http://localhost:16125/dlcm/docs/DLCM-ToolsGuide.html#dlcm-tools-get[DLCM Tools]
** for `standalone` deployment http://localhost:16135/dlcm/docs/DLCM-ToolsGuide.html#dlcm-tools-get[DLCM Tools]
** for `multi-storage` deployment http://localhost:16145/dlcm/docs/DLCM-ToolsGuide.html#dlcm-tools-get[DLCM Tools]

=== Run the batch tool
* Go to the `properties` folder of the corresponding deployment
```
cd <deployment>/properties
```
* Run the tool by command line
```
java 
  -jar DLCM-Tools-x.x.x.jar <1> 
  --spring.cloud.bootstrap.enabled=true 
  --spring.cloud.bootstrap.location=dlcm-tools.properties <2> 
  --dlcm.import=<Folder with datasets> <3> 
  --solidify.oauth2.accesstoken=<User token> <4>
```
<1> The DLCM Tools downloaded previously
<2> The file is in `<deployment>/properties` folder
<3> The folder where the `Dataset` repository was cloned
<4> The OAuth2 token of the user, available in `user menu` of DLCM portal

== Environment reset
* Delete all containers
* Delete all volumes with the command `docker volume prune`

== Known issues
* If a VPN is enable, the network creation could not work.
