#!/bin/bash
uid=5000
gid=5000
prefix="light"

# Check if IP_ADDRESS is set
if [[ -z "$IP_ADDRESS" ]]; then
    echo "IP_ADDRESS is not defined!"
    exit
else
   echo "IP_ADDRESS is $IP_ADDRESS"
   echo "===================================================="
fi

# Create volumes for storage
createStorage(){
    local_prefix=$1
    local_num=$2
    local_storage_normal=dlcm-$local_prefix-storagion$local_num-normal-storage-volume
    docker volume create $local_storage_normal
    docker run --rm -v $local_storage_normal:/data busybox sh -c "chown -R $uid:$gid /data"

    local_storage_secured=dlcm-$local_prefix-storagion$local_num-secured-storage-volume
    docker volume create $local_storage_secured
    docker run --rm -v $local_storage_secured:/data busybox sh -c "chown -R $uid:$gid /data"
}


echo "Update configuration files:"
sed -i "s/%IP_ADDRESS%/${IP_ADDRESS}/g" ./portal_config/environment.runtime.json
sed -i "s/%IP_ADDRESS%/${IP_ADDRESS}/g" ./properties/dlcm-demo-data.properties
sed -i "s/%IP_ADDRESS%/${IP_ADDRESS}/g" ./properties/dlcm-tools.properties

echo "Create docker volume for home:"
dlcm_home=dlcm-$prefix-home-volume
docker volume create $dlcm_home
docker run --rm -v $dlcm_home:/data busybox sh -c "chown -R $uid:$gid /data"

echo "Create docker volume for storagion1:"
createStorage $prefix 1

echo "Create docker volume for storagion2:"
createStorage $prefix 2

echo "Create docker volume for portal:"
dlcm_portal=dlcm-$prefix-portal-volume
docker volume create $dlcm_portal

docker create -v $dlcm_portal:/data --name portal busybox
docker cp portal_config/. portal:data
docker rm portal
docker run --rm -v $dlcm_portal:/data busybox sh -c "chown -R 101:101 /data"
