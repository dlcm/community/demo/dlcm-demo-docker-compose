#!/bin/bash

# Check if IP_ADDRESS is set
if [[ -z "$IP_ADDRESS" ]]; then
    echo "IP_ADDRESS is not defined!"
    exit
else
   echo "IP_ADDRESS is $IP_ADDRESS"
   echo "===================================================="
fi

# Configserver
echo "Create docker volume for configserver:"
docker volume create configserver-config-volume
docker volume create configserver-ssh-volume

docker create -v configserver-config-volume:/data --name copier busybox
docker cp configserver/. copier:data
docker rm copier
docker run --rm -v configserver-config-volume:/data busybox sh -c "chown -R 5000:5000 /data"

docker create -v configserver-ssh-volume:/data --name copier busybox
docker cp configserver/.ssh copier:data
docker rm copier

# Shib simulator
echo "Create docker volume for Shib simulator:"
docker volume create shibsimulator-config-volume
docker volume create shibsimulator-templates-volume
docker volume create shibsimulator-img-volume

docker create -v shibsimulator-config-volume:/data --name copier busybox
docker cp shibsimulator/config/. copier:data
docker rm copier

docker create -v shibsimulator-templates-volume:/data --name copier busybox
docker cp shibsimulator/templates/. copier:data
docker rm copier

docker volume create shibsimulator-img-volume
docker create -v shibsimulator-img-volume:/data --name copier busybox
docker cp shibsimulator/img/. copier:data
docker rm copier

# Authorization server
echo "Create docker volume for authorization server:"
docker volume create authorization-config-volume

docker create -v authorization-config-volume:/data --name copier busybox
docker cp authorization/. copier:data
docker rm copier
docker run --rm -v authorization-config-volume:/data busybox sh -c "chown -R 5000:5000 /data"
